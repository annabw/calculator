## Calculator  
Simple PHP calculator with operations plus and minus. Calculator is open for more functions.
Calculator written in PHP7, Laravel 5.8 and JavaScript. 


#### Target 
* using namespaces and PHP7 features  
* follow OOP and SOLID principles  
* catch all necessary exceptions  
* create unit tests