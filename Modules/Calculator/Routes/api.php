<?php

Route::group(['middleware' => 'api'], function()
{
    Route::post('/result','Api\CalculatorController@calculate')->name('result');
});
