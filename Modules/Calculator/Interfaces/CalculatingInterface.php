<?php

namespace Modules\Calculator\Interfaces;

interface CalculatingInterface 
{
    public function calculate();
}