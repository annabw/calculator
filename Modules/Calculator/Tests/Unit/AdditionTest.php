<?php

namespace Modules\Calculator\Tests\Unit;

use Tests\TestCase;


class AdditionTest extends TestCase
{
    /* test addition */
    public function testAddsUpGivenNumbers()
    {
        $addition = new \Modules\Calculator\Entities\Addition;
        $addition->setNumbers([1,1]);

        $this->assertEquals(2, $addition->calculate());
    }
    
    //@todo more tests
}
