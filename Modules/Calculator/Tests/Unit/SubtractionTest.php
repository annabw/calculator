<?php

namespace Modules\Calculator\Tests\Unit;

use Tests\TestCase;


class SubtractionTest extends TestCase
{
    /* test subtraction */
    public function testSubtractGivenNumbers()
    {
        $subtraction = new \Modules\Calculator\Entities\Subtraction;
        $subtraction->setNumbers([10,5]);

        $this->assertEquals(5, $subtraction->calculate());
    }
    
    //@todo more tests
}