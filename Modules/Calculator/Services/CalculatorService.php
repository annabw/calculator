<?php

namespace Modules\Calculator\Services;

class CalculatorService
{ 
    public function operations() 
    {    
        $operationsConfig = config('operations.standard');
        $operations = [];
        if(isset($operationsConfig)) {
            foreach ($operationsConfig as $operationKey => $operationValue) {
                $operations[$operationKey]['operation'] = $operationValue[0];
                $operations[$operationKey]['unicode'] = $operationValue[2];
            }
        }
        return $operations;
    }

    public static function errorMessage() 
    {    
        return 'Sorry, this function will work when you purchase full access';
    }
}