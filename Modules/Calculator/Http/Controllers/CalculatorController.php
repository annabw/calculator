<?php

namespace Modules\Calculator\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\Calculator\Services\CalculatorService;

class CalculatorController extends Controller
{
    public function index()
    {
        $operations = App::make(CalculatorService::class);
        $operations = $operations->operations();
        return view('calculator::index', compact('operations'));
    }

}
