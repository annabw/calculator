<?php

namespace Modules\Calculator\Entities;

use Modules\Calculator\Interfaces\CalculatingInterface;

class Addition implements CalculatingInterface
{
    protected $numbers;

    public function setNumbers(array $numbers) {

        $this->numbers = $numbers;
    }

    public function calculate() : float
    {
        return array_sum($this->numbers);
    }
}
