<?php
return [
    'standard' => [
        'Addition' => ['+','plus',"\u{002B}"],
        'Subtraction' => ['-','minus',"\u{0006}"],
        'Multiplication' => ['*','times',"\u{00D7}"],
        'Division' => ['/','obelus',"\u{00F7}"]
    ]
];

